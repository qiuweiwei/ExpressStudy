const env = require('./env')

module.exports =(function () {

    var node_env = process.env.NODE_ENV||'development';
    var defaultConfig = env['default']
    for(var key in env[node_env]){

        defaultConfig[key] = env[node_env][key]
    }
    return defaultConfig

})()


